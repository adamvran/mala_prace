# Malá práce


# Stránky
https://adamvran.gitlab.io/mala_prace/

# Run
1. Vytvoření a aktivace virtuálního prostředí

```
virtualenv venv
source venv/bin/activate
```

1. Instalace requirements.txt

``` 
pip install -r requirements.txt
```

1. Spuštění serveru

```
mkdocs serve
```
